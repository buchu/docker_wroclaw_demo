FROM python:latest
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

ADD requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ADD . /code
WORKDIR /code/docker_demo
CMD ["/code/docker-entrypoint.sh"]